package kedziak.dominik.allegrosummerintern.base;

import androidx.lifecycle.ViewModel;

import kedziak.dominik.allegrosummerintern.data.GitRepository;
import kedziak.dominik.allegrosummerintern.di.Component.DaggerRepositoryComponent;
import kedziak.dominik.allegrosummerintern.di.Component.RepositoryComponent;
import kedziak.dominik.allegrosummerintern.di.Modules.RetrofitModule;

public abstract class BaseViewModel extends ViewModel {

    private GitRepository mRepository;

    public BaseViewModel() {
        RepositoryComponent component =
                DaggerRepositoryComponent
                        .builder()
                        .retrofitModule(new RetrofitModule())
                        .build();
        this.mRepository = component.getRepository();
    }

    public GitRepository getRepository() {
        return mRepository;
    }

}
