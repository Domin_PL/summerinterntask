package kedziak.dominik.allegrosummerintern.data;

import javax.inject.Inject;

import io.reactivex.Single;
import kedziak.dominik.allegrosummerintern.data.model.Items;
import kedziak.dominik.allegrosummerintern.network.RetrofitClient;

public class GitRepository  {

    private RetrofitClient mClient;

    @Inject
    public GitRepository(RetrofitClient mClient) {
        this.mClient = mClient;
    }

    public Single<Items> getLatestRepos(){
        return mClient.getRecentRepos();
    }

}
