package kedziak.dominik.allegrosummerintern.data.model;


import com.squareup.moshi.Json;

/**
 * Model to obtain and store latest updated GitHub's repositories
 * and just its name and description
 */
public class GitRepo {

    private static final String name = "name";

    public GitRepo(String mName) {
        this.mName = mName;
    }

    @Json(name = name)
    private String mName;

    public String getName() {
        return mName;
    }
}
