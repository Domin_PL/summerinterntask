package kedziak.dominik.allegrosummerintern.data.model;

import com.squareup.moshi.Json;

import java.util.List;

public class Items {

    public Items(List<GitRepo> list) {
        this.list = list;
    }

    public List<GitRepo> getList() {
        return list;
    }

    @Json(name = "items")
    private List<GitRepo> list;

}
