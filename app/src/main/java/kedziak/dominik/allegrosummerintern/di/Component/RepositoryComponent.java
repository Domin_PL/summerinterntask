package kedziak.dominik.allegrosummerintern.di.Component;

import dagger.Component;
import kedziak.dominik.allegrosummerintern.data.GitRepository;
import kedziak.dominik.allegrosummerintern.di.Modules.RetrofitModule;
import kedziak.dominik.allegrosummerintern.network.GithubService;
import kedziak.dominik.allegrosummerintern.network.RetrofitClient;

@Component(modules = RetrofitModule.class)
public interface RepositoryComponent {

    GitRepository getRepository();
    RetrofitClient getRetrofitClient();
    GithubService getService();

}
