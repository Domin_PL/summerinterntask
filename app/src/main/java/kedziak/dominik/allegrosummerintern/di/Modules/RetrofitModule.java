package kedziak.dominik.allegrosummerintern.di.Modules;

import dagger.Module;
import dagger.Provides;
import kedziak.dominik.allegrosummerintern.network.GithubService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class RetrofitModule {

    private GithubService service;

    private static final String GITHUB_BASE_URL = "https://api.github.com/";

    public RetrofitModule() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GITHUB_BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
        service = retrofit.create(GithubService.class);
    }

    @Provides
    public GithubService getGitService(){
        return service;
    }
}
