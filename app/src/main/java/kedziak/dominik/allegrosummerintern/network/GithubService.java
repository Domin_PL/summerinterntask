package kedziak.dominik.allegrosummerintern.network;

import io.reactivex.Single;
import kedziak.dominik.allegrosummerintern.data.model.Items;
import retrofit2.http.GET;

public interface GithubService {

    /**
     * This interface replaces parameter username with argument name and searches
     * for List with latest updated repos
     * @return List containing objects with names and descriptions of latest updated objects
     */

    @GET("search/repositories?q=org:Allegro&sort=updated&order=desc")
    Single<Items> getRepos();

}
