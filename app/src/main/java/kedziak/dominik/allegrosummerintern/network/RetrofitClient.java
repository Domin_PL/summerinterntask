package kedziak.dominik.allegrosummerintern.network;

import javax.inject.Inject;

import io.reactivex.Single;
import kedziak.dominik.allegrosummerintern.data.model.Items;

public class RetrofitClient {

    private GithubService mService;

    @Inject
    public RetrofitClient(GithubService service) {
        this.mService = service;
    }

    public Single<Items> getRecentRepos(){
        return mService.getRepos();
    }

}
