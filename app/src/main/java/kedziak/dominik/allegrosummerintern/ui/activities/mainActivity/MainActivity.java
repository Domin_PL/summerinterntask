package kedziak.dominik.allegrosummerintern.ui.activities.mainActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import kedziak.dominik.allegrosummerintern.R;
import kedziak.dominik.allegrosummerintern.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init(){
        ActivityMainBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_main);
        MainViewModel mViewModel = ViewModelProviders.of(this)
                .get(MainViewModel.class);
        binding.setViewModel(mViewModel);
    }
}
