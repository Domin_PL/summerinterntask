package kedziak.dominik.allegrosummerintern.ui.activities.mainActivity;

import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.allegrosummerintern.base.BaseViewModel;
import kedziak.dominik.allegrosummerintern.data.GitRepository;

public class MainViewModel extends BaseViewModel {

    private GitRepository mRepository;
    public ObservableField<String> name = new ObservableField<>();
    public ObservableInt isExecuted = new ObservableInt();

    private Disposable disposable;

    public MainViewModel() {
        this.mRepository = getRepository();
        isExecuted.set(View.GONE);
    }

    public void getRepos(){
        isExecuted.set(View.VISIBLE);
        disposable = mRepository.getLatestRepos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        items -> {
                            name.set(items.getList().get(0).getName());
                            hideProgressBar();
                            closeDisposables();
                        },
                        failure -> {
                            Log.e("MainViewModelOnFailure", failure.getMessage(), failure);
                            hideProgressBar();
                            closeDisposables();
                        }
                );
    }

    private void hideProgressBar(){
        isExecuted.set(View.GONE);
    }

    private void closeDisposables(){
        if (!disposable.isDisposed())
            disposable.dispose();
    }

}
