package kedziak.dominik.allegrosummerintern.network;

import android.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kedziak.dominik.allegrosummerintern.data.model.GitRepo;
import kedziak.dominik.allegrosummerintern.data.model.Items;

import static org.junit.Assert.assertEquals;

public class RetrofitClientTest {

    private Disposable mDisposable;

    @Mock private GithubService service;

    private Items items;

    private static final String name = "swift-junit";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        GitRepo mRepositories = new GitRepo(name);
        List<GitRepo> mReposList = new ArrayList<>();
        mReposList.add(mRepositories);
        items = new Items(mReposList);
    }


    @Test
    public void getRecentRepos() {
        SingleOnSubscribe<Items> singleOnSubscribe = emitter -> emitter.onSuccess(items);
        Single<Items> single = Single.create(singleOnSubscribe);

        Mockito.when(service.getRepos()).thenReturn(single);

        mDisposable = service.getRepos()
            .subscribeOn(Schedulers.io())
            .subscribe(
                    mList -> {
                        GitRepo mObject = mList.getList().get(0);
                        String mObjectName = mObject.getName();
                        assertEquals(name, mObjectName);
                        },
                    failure ->  Log.d("onFailure", failure.getMessage(), failure)
        );
    }

    @After
    public void clearDisposable(){
        if (!mDisposable.isDisposed())
        mDisposable.dispose();
    }
}